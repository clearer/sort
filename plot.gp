files = "sorted reverse random semi_sorted cut"
set terminal png size 1280,1280
set ytics nomirror
set zero 1e-16

name = system("echo $NAME");
plot_title = system("echo $NAME | sed 's/_/ /g'");
unset output

set style line 1 lc rgb "red" linewidth 2
set style line 2 lc rgb "blue" linewidth 2
set style line 3 lc rgb "orange" linewidth 2
set style line 4 lc rgb "green" linewidth 2
set yrange [0:]

set output "graf/" . name . "-sammenligninger.png"
set title "Sammenligninger"
plot\
	"data/" . name . ".data" using ($1):($2 / ceil($1 * log($1) / log(2))) title "ny sort" with lines ls 1, \
	"data/" . name . ".data" using ($1):($5 / ceil($1 * log($1) / log(2))) title "mergesort" with lines ls 2, \
	"data/" . name . ".data" using ($1):($8 / ceil($1 * log($1) / log(2))) title "blocksort" with lines ls 3

set output "graf/" . name . "-tid.png"
set title "Tid"
plot\
	"data/" . name . ".data" using ($1):($3 / ceil($1 * log($1) / log(2))) title "ny sort" with lines ls 1, \
	"data/" . name . ".data" using ($1):($6 / ceil($1 * log($1) / log(2))) title "mergesort" with lines ls 2, \
	"data/" . name . ".data" using ($1):($9 / ceil($1 * log($1) / log(2))) title "blocksort" with lines ls 3

set output "graf/" . name . "-assignments.png"
set title "assignments"
plot\
	"data/" . name . ".data" using ($1):($4 / ceil($1 * log($1) / log(2))) title "ny sort" with lines ls 1, \
	"data/" . name . ".data" using ($1):($7 / ceil($1 * log($1) / log(2))) title "mergesort" with lines ls 2, \
	"data/" . name . ".data" using ($1):($10 / ceil($1 * log($1) / log(2))) title "blocksort" with lines ls 3, \
	"data/" . name . ".data" using ($1):($13 / ceil($1 * log($1) / log(2))) title "min" with lines ls 4

