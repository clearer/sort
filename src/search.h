#ifndef UUID_BA44800B_63D1_43BF_A7AE_9CDD760DD8CB
#define UUID_BA44800B_63D1_43BF_A7AE_9CDD760DD8CB

template<class I, class T, class C>
I lower_bound(I begin, I end, T target, C comp = std::less_equal<T>()) {
	I mid;
	while(begin != end && comp(*begin++, target) == false) {
		mid = begin + (end - begin) / 2;
		if (comp(*mid, target)) begin = mid;
		else end = mid;
	}

	return begin;
}


template<class I, class T, class C>
I upper_bound(I begin, I end, T target, C comp = std::less_equal<T>()) {
	I mid;
	end = end - 1;
	while(begin != end && comp(*end--, target) == false) {
		mid = begin + (end - begin) / 2;
		if (comp(*mid, target)) end = mid;
		else begin = mid;
	}

	return end;
}

#endif
