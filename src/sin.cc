#include <random>
#include <vector>
#include <cmath>
#include "integer.h"

std::vector<integer> build_vector(std::size_t length) {
	std::vector<integer> a(length);
	double r[3] { 0.0,  0.0,  0.0};
	double s[3] { 1.0,  -1.0,  2.0};
	double p[3] { 0.1,  0.2,  0.1};

	for (auto i = 0; i < length; ++i) {
		a[i] = s[0] * sin(r[0]) + s[1] * tan(r[1]) + s[2] * tan(r[2]);
		r[0] += p[0];
		r[1] += p[0];
		r[2] += p[0];
	}

	return a;
}

