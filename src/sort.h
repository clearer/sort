#ifndef UUID_687f9416_f265_4ad0_bbf2_50ae4e7a5931
#define UUID_687f9416_f265_4ad0_bbf2_50ae4e7a5931

#include <iterator>
#include <iostream>
#include <algorithm>
#include <cstddef>
#include <functional>
#include "integer.h"
#include "insertionsort.h"
#include "merge.h"

template<class I>
void csort(I begin, I end, I s) {
	typedef typename std::iterator_traits<I>::value_type V;

	if (end - begin < 2) return;

	if (s <= begin) {
		auto r = std::is_sorted_until(s, end, std::greater<V>());
		std::reverse(s, r);
		if (*s <= *(s + 1)) s = r;
		s = std::is_sorted_until(s - 1, end);
	}

	if (s == end) return;

	if (end - begin < limit) {
		insertionsort(begin, end, s);
		return;
	}

	I mid = begin + (end - begin) / 2;
	csort(begin, mid, s);
	csort(mid, end, std::max(mid, s));
	if (*(mid - 1) <= *mid) return;
	begin = lower_bound(begin, mid, *mid);
	if (*(end - 1) < *begin) {
		std::rotate(begin, mid, end);
		return;
	}

	merge(begin, mid, end);
}

template<class I>
void csort(I begin, I end)  {
	csort(begin, end, begin);
}

#endif
