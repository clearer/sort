#include <algorithm>
#include <random>
#include <vector>
#include "integer.h"

std::vector<integer> build_vector(std::size_t length) {
	std::vector<integer> a(length);
	std::generate(begin(a), end(a), std::rand);
	return a;
}

