#include <cstring>
#include <cfloat>
#include <stdexcept>
#include <vector>
#include <iomanip>
#include <ios>
#include <iostream>
#include <algorithm>
#include <sys/time.h>
#include <sys/resource.h>
#include <unistd.h>

#include "integer.h"
#include "sort.h"
#include "mergesort.h"
#include "blocksort.h"
#include "minsort.h"

using namespace std;

std::vector<integer> build_vector(size_t);
std::vector<integer> build_vector(size_t a, size_t b) {
	return build_vector(a);
}

unsigned long long integer::comparisons = 0;
unsigned long long integer::assignments = 0;
size_t iterations = 5;

template<class I>
double time(void (*f)(I, I), I begin, I end) {
	struct rusage resources;
	integer::comparisons = 0;
	integer::assignments = 0;
	timeval start_time;
	timeval end_time;
	getrusage(RUSAGE_SELF, &resources);
	start_time.tv_sec = resources.ru_utime.tv_sec;
	start_time.tv_usec = resources.ru_utime.tv_usec;
	f(begin, end);
	getrusage(RUSAGE_SELF, &resources);
	end_time.tv_sec = resources.ru_utime.tv_sec;
	end_time.tv_usec = resources.ru_utime.tv_usec;

	auto i = integer::comparisons;
	auto a = integer::assignments;

	if (is_sorted(begin, end) == false)
		throw runtime_error("failed to sort!");

	integer::comparisons = i;
	integer::assignments = a;

	return (1000000.f * 
		double(end_time.tv_sec - start_time.tv_sec) + 
		double(end_time.tv_usec - start_time.tv_usec)) / 1000000.f;
}

int main(int argc, char ** argv) {
	int factor = 1;
	size_t mergesort_compare = 0;
	size_t nysort_compare = 0;
	size_t blocksort_compare = 0;
	size_t minsort_compare = 0;
	size_t minsort_assignments = 0;

	size_t mergesort_assignments = 0;
	size_t nysort_assignments = 0;
	size_t blocksort_assignments = 0;

	double mergesort_time = 0.0;
	double blocksort_time = 0.0;
	double nysort_time = 0.0;
	double minsort_time = 0;

	std::vector<integer> a;
	std::vector<integer> b;

	{
		timeval seed;
		gettimeofday(&seed, 0);
		srand(seed.tv_usec);
	}

	for (size_t n = 10000; n < 20000; n *= 1.03) {
		cerr << n << "\r";
		cout << n;

		blocksort_compare = 0;
		blocksort_time = 0;
		mergesort_compare = 0;
		mergesort_time = 0;
		nysort_compare = 0;
		nysort_time = 0;
		nysort_assignments = 0;
		blocksort_assignments = 0;
		mergesort_assignments = 0;
		minsort_compare = 0;
		minsort_time = 0;
		minsort_assignments = 0;
	
		b.resize(n);

		for (size_t l = 0; l < iterations; ++l) {
			a = build_vector(n, factor);

			// merge sort
			copy(a.begin(), a.end(), b.begin());
			blocksort_time        += time(blocksort, begin(b), end(b));
			blocksort_compare     += integer::comparisons;
			blocksort_assignments += integer::assignments;

			// merge sort
			copy(a.begin(), a.end(), b.begin());
			mergesort_time        += time(merge_sort, begin(b), end(b));
			mergesort_compare     += integer::comparisons;
			mergesort_assignments += integer::assignments;

			// nysort
			nysort_time        += time(csort, begin(a), end(a));
			nysort_compare     += integer::comparisons;
			nysort_assignments += integer::assignments;
			copy(a.begin(), a.end(), b.begin());

			minsort_time        += time(minsort, begin(b), end(b));
			minsort_compare     += integer::comparisons;
			minsort_assignments += integer::assignments;

		}

		cout << " " << nysort_compare / iterations << " " << nysort_time / iterations << " " << nysort_assignments / iterations;
		cout << " " << mergesort_compare  / iterations << " " << mergesort_time / iterations << " " << mergesort_assignments / iterations;
		cout << " " << blocksort_compare / iterations << " " << blocksort_time / iterations << " " << blocksort_assignments / iterations;;
		cout << " " << minsort_compare / iterations << " " << minsort_time / iterations << " " << minsort_assignments / iterations;;

		cout << endl;
	}
}

