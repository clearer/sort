#ifndef UUID_8c220322_9ab1_4aa0_8c7e_54f2b9c1dd48
#define UUID_8c220322_9ab1_4aa0_8c7e_54f2b9c1dd48

#include <algorithm>
#include "search.h"

enum {limit = 32};

template<class I>
void insertionsort(I begin, I end, I sorted) {
	if (sorted == begin) sorted = std::is_sorted_until(begin, end);
	for (auto i = sorted; i < end; i ++) {	
		auto j = lower_bound(begin, i, *i);
		if (j != i) {
			auto temp = *i;
			std::copy_backward(j, i, i + 1);
			*j = temp;
		}
	}
}

#endif
