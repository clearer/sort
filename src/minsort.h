#include <algorithm>
#include <iterator>

template<class I>
void minsort(I begin, I end) {
	I min = begin;
	for (I i = begin; i < end; ++i) {
		I min = i;
		for (I j = i + 1; j < end; ++j) if (*j < *min) min = j;
		if (i != min) {
			auto t = *i;
			*i = *min;
			*min = t;
		}
	}
}
