#include <vector>
#include "integer.h"

std::vector<integer> build_vector(std::size_t length) {
	std::vector<integer> a(length);
	for (std::size_t i = 0; i < length; ++i) 
		a[i] = i  * (i % 2) == 0 - i *((i % 2));
	return a;
}

