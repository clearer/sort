#include <random>
#include <vector>
#include <cmath>
#include "integer.h"

std::vector<integer> cutter(std::size_t length, std::size_t cuts) {
	std::vector<integer> a(length);
	std::size_t l;
	std::size_t n = 0;
	std::size_t r;

	while (n < length) {
		l = std::size_t(rand() % (length - n - cuts)) ;
		l = std::max(std::size_t(1), l);
		if (cuts == 1) l = length - n;
		r = rand();
		for (std::size_t k = 0; k < l; ++k)
			a[n++] = r++;
		--cuts;
	}
	return a;
}

