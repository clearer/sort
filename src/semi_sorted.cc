#include <random>
#include <vector>
#include "integer.h"

std::vector<integer> build_vector(std::size_t length) {
	std::vector<integer> a(length);
	std::size_t k = 0;
	for (std::size_t i = 0; i < length; ++i) {
		k = rand();
		std::size_t n = rand() % (length - i);
		for (std::size_t j = 0; j < n ; j++)
		a[i + j] = k++;
		i+=n;
	}
	return a;
}

