#ifndef UUID_cc650ecc_396c_4c62_88ab_9315bedfa330
#define UUID_cc650ecc_396c_4c62_88ab_9315bedfa330

#include <algorithm>

template<class I>
void merge_sort(I begin, I end) {
	std::size_t m = 1;
	std::size_t n = end - begin;

	for (std::size_t m = 0; m < n; m += limit)
		insertionsort(begin + m, std::min(begin + m + limit, end), 
			begin + m);

	for (std::size_t m = limit; m <= n; m *= 2) {
		for (std::size_t i = 0; i < n - m; i += 2 * m) {
			I stop = std::min(begin + i + 2 * m, end);
			I mid = begin + i + m;
			I start = begin + i;
			if (*(mid - 1) > *mid) merge(start, mid, stop);
		}	
	}
}

#endif
