#ifndef INTEGER_H
#define INTEGER_H

class integer {
	int i;
public:
	static unsigned long long comparisons;
	static unsigned long long assignments;
	integer(int _i = 0) : i(_i) {}
	operator int() const { return i; }
	bool operator < (integer const& _i) const { ++comparisons; return i < _i.i; }
	bool operator <= (integer const& _i) const { ++comparisons; return i <= _i.i; }
	bool operator > (integer const& _i) const { ++comparisons; return i > _i.i; }
	bool operator >= (integer const& _i) const { ++comparisons; return i >= _i.i; }
	bool operator == (integer const& _i) const { ++comparisons; return i == _i.i; }
	bool operator != (integer const& _i) const { ++comparisons; return i != _i.i; }

	integer& operator = (integer const& _i)  {  ++assignments; i = _i.i; return *this; }
};

std::vector<integer> cutter(std::size_t,std::size_t);

#endif
