/***********************************************************
 WikiSort: a public domain implementation of "Block Sort"
 https://github.com/BonzaiThePenguin/WikiSort
 to run:
 clang++ -o WikiSort.x WikiSort.cpp -O3
 (or replace 'clang++' with 'g++')
 ./WikiSort.x

insertion sort implementeringeren skiftet ud med den samme
som de andre algoritmer bruger. Ellers er koden der bliver
brugt den samme.

***********************************************************/

#include <algorithm>
#include <cassert>
#include <cmath>
#include <ctime>
#include <iostream>
#include <iterator>
#include <limits>
#include <vector>
#include "insertionsort.h"
#include "search.h"



// use a class so the memory for the cache is freed when the object goes out of scope,
// regardless of whether exceptions were thrown (only needed in the C++ version)
template <typename T>
class Cache {
public:
	T *cache;
	size_t cache_size;
	
	~Cache() {
		if (cache) delete[] cache;
	}
	
	Cache(size_t size) {
		// good choices for the cache size are:
		// (size + 1)/2 – turns into a full-speed standard merge sort since everything fits into the cache
		cache_size = (size + 1)/2;
		cache = new (std::nothrow) T[cache_size];
		if (cache) return;

		// sqrt((size + 1)/2) + 1 – this will be the size of the A blocks at the largest level of merges,
		// so a buffer of this size would allow it to skip using internal or in-place merges for anything
		cache_size = sqrt(cache_size) + 1;
		cache = new (std::nothrow) T[cache_size];
		if (cache) return;
		
		// 512 – chosen from careful testing as a good balance between fixed-size memory use and run time
		if (cache_size > 512) {
			cache_size = 512;
			cache = new (std::nothrow) T[cache_size];
			if (cache) return;
		}
		
		// 0 – if the system simply cannot allocate any extra memory whatsoever, no memory works just fine
		cache_size = 0;
	}
};

// structure to represent ranges within the array
template <typename Iterator>
struct Range {
	Iterator start;
	Iterator end;

	Range() {}

	Range(Iterator start, Iterator end):
		start(start),
		end(end)
	{}

	std::size_t length() const {
		return std::distance(start, end);
	}
};

// toolbox functions used by the sorter

// 63 -> 32, 64 -> 64, etc.
// this comes from Hacker's Delight
template <typename Unsigned>
Unsigned Hyperfloor(Unsigned value) {
	for (std::size_t i = 1 ; i <= std::numeric_limits<Unsigned>::digits / 2 ; i <<= 1) {
		value |= (value >> i);
	}
	return value - (value >> 1);
}

// combine a linear search with a binary search to reduce the number of Cs in situations
// where have some idea as to how many unique values there are and where the next value might be
template <typename I, typename T, typename C>
I FindFirstForward(I first, I last,
									  const T & value, C compare, std::size_t unique) {
	std::size_t size = std::distance(first, last);
	if (size == 0) return first;

	std::size_t skip = std::max(size / unique, (std::size_t)1);

	I index;
	for (index = first + skip ; compare(*(index - 1), value) ; index += skip) {
		if (index >= last - skip) {
			return std::lower_bound(index, last, value, compare);
		}
	}
	return std::lower_bound(index - skip, index, value, compare);
}

template <typename I, typename T, typename C>
I FindLastForward(I first, I last, const T & value, C compare, std::size_t unique) {
	std::size_t size = std::distance(first, last);
	if (size == 0) return first;

	std::size_t skip = std::max(size / unique, (std::size_t)1);

	I index;
	for (index = first + skip ; !compare(value, *(index - 1)) ; index += skip) {
		if (index >= last - skip) {
			return std::upper_bound(index, last, value, compare);
		}
	}

	return std::upper_bound(index - skip, index, value, compare);
}

template <typename I, typename T, typename C>
I FindFirstBackward(I first, I last,
									   const T & value, C compare, std::size_t unique) {
	std::size_t size = std::distance(first, last);
	if (size == 0) return first;

	std::size_t skip = std::max(size / unique, (std::size_t)1);

	I index;
	for (index = last - skip ; index > first && !compare(*(index - 1), value) ; index -= skip) {
		if (index < first + skip) {
			return std::lower_bound(first, index, value, compare);
		}
	}
	return std::lower_bound(index, index + skip, value, compare);
}

template <typename I, typename T, typename C>
I FindLastBackward(I first, I last,
									  const T & value, C compare, std::size_t unique) {
	std::size_t size = std::distance(first, last);
	if (size == 0) return first;

	std::size_t skip = std::max(size / unique, (std::size_t)1);

	I index;
	for (index = last - skip ; index > first && compare(value, *(index - 1)) ; index -= skip) {
		if (index < first + skip) {
			return std::upper_bound(first, index, value, compare);
		}
	}
	return std::upper_bound(index, index + skip, value, compare);
}

// merge operation using an external buffer
template <typename I1, typename I2, typename C>
void MergeExternal(I1 first1, I1 last1,
				   I1 first2, I1 last2,
				   I2 cache, C compare) {
	// A fits into the cache, so use that instead of the internal buffer
	I2 A_index = cache;
	I2 A_last = cache + std::distance(first1, last1);
	I1 B_index = first2;
	I1 B_last = last2;
	I1 insert_index = first1;

	if (last2 - first2 > 0 && last1 - first1 > 0) {
		while (true) {
			if (!compare(*B_index, *A_index)) {
				*insert_index = *A_index;
				++A_index;
				++insert_index;
				if (A_index == A_last) break;
			} else {
				*insert_index = *B_index;
				++B_index;
				++insert_index;
				if (B_index == B_last) break;
			}
		}
	}

	// copy the remainder of A into the final array
	std::copy(A_index, A_last, insert_index);
}

// merge operation using an internal buffer
template<typename I, typename C>
void MergeInternal(I first1, I last1,
				   I first2, I last2,
				   I buffer, C compare) {
	// whenever we find a value to add to the final array, swap it with the value that's already in that spot
	// when this algorithm is finished, 'buffer' will contain its original contents, but in a different order
	I A_index = buffer;
	I B_index = first2;
	I A_last = buffer + std::distance(first1, last1);
	I B_last = last2;
	I insert_index = first1;

	if (last2 - first2 > 0 && last1 - first1 > 0) {
		while (true) {
			if (!compare(*B_index, *A_index)) {
				std::iter_swap(insert_index, A_index);
				++A_index;
				++insert_index;
				if (A_index == A_last) break;
			} else {
				std::iter_swap(insert_index, B_index);
				++B_index;
				++insert_index;
				if (B_index == B_last) break;
			}
		}
	}

	// BlockSwap
	std::swap_ranges(A_index, A_last, insert_index);
}

// merge operation without a buffer
template <typename I, typename C>
void MergeInPlace(I first1, I last1,
				  I first2, I last2,
				  C compare) {
	if (last1 - first1 == 0 || last2 - first2 == 0) return;

	while (true) {
		I mid = std::lower_bound(first2, last2, *first1, compare);

		// rotate A into place
		std::size_t amount = mid - last1;
		std::rotate(first1, last1, mid);
		if (last2 == mid) break;

		// calculate the new A and B ranges
		first2 = mid;
		first1 += amount;
		last1 = first2;
		first1 = std::upper_bound(first1, last1, *first1, compare);
		if (std::distance(first1, last1) == 0) break;
	}
}

class Iterator {
	std::size_t size, power_of_two;
	std::size_t decimal, numerator, denominator;
	std::size_t decimal_step, numerator_step;

public:

	Iterator(std::size_t size, std::size_t min_level):
		size(size),
		power_of_two(Hyperfloor(size)),
		decimal(0),
		numerator(0),
		denominator(power_of_two / min_level),
		decimal_step(size / denominator),
		numerator_step(size % denominator)
	{}

	void begin() {
		numerator = decimal = 0;
	}

	template <typename Iterator>
	Range<Iterator> nextRange(Iterator it) {
		std::size_t start = decimal;

		decimal += decimal_step;
		numerator += numerator_step;
		if (numerator >= denominator) {
			numerator -= denominator;
			++decimal;
		}

		return Range<Iterator>(it + start, it + decimal);
	}

	bool finished() const {
		return decimal >= size;
	}

	bool nextLevel() {
		decimal_step += decimal_step;
		numerator_step += numerator_step;
		if (numerator_step >= denominator) {
			numerator_step -= denominator;
			++decimal_step;
		}

		return decimal_step < size;
	}

	std::size_t length() const {
		return decimal_step;
	}
};

template <typename I>
void blocksort(I first, I last) {
	typedef typename std::iterator_traits<I>::value_type V;
	blocksort(first, last, std::less<V>());
}

template <typename I, typename C>
void blocksort(I first, I last, C compare) {
	typedef typename std::iterator_traits<I>::value_type T;
	const std::size_t size = std::distance(first, last);

	if (size < 4) {
		if (size == 3) {
			if (compare(first[1], first[0])) {
				std::iter_swap(first + 0, first + 1);
			}
			if (compare(first[2], first[1])) {
				std::iter_swap(first + 1, first + 2);
				if (compare(first[1], first[0])) {
					std::iter_swap(first + 0, first + 1);
				}
			}
		} else if (size == 2) {
			if (compare(first[1], first[0])) {
				std::iter_swap(first + 0, first + 1);
			}
		}

		return;
	}

	Iterator iterator (size, 4);
	while (!iterator.finished()) {
		int order[] = { 0, 1, 2, 3, 4, 5, 6, 7 };
		Range<I> range = iterator.nextRange(first);

		#define SWAP(x, y) \
			if (compare(range.start[y], range.start[x]) || \
				(order[x] > order[y] && !compare(range.start[x], range.start[y]))) { \
				std::iter_swap(range.start + x, range.start + y); \
				std::iter_swap(order + x, order + y); }

		if (range.length() == 8) {
			SWAP(0, 1); SWAP(2, 3); SWAP(4, 5); SWAP(6, 7);
			SWAP(0, 2); SWAP(1, 3); SWAP(4, 6); SWAP(5, 7);
			SWAP(1, 2); SWAP(5, 6); SWAP(0, 4); SWAP(3, 7);
			SWAP(1, 5); SWAP(2, 6);
			SWAP(1, 4); SWAP(3, 6);
			SWAP(2, 4); SWAP(3, 5);
			SWAP(3, 4);

		} else if (range.length() == 7) {
			SWAP(1, 2); SWAP(3, 4); SWAP(5, 6);
			SWAP(0, 2); SWAP(3, 5); SWAP(4, 6);
			SWAP(0, 1); SWAP(4, 5); SWAP(2, 6);
			SWAP(0, 4); SWAP(1, 5);
			SWAP(0, 3); SWAP(2, 5);
			SWAP(1, 3); SWAP(2, 4);
			SWAP(2, 3);

		} else if (range.length() == 6) {
			SWAP(1, 2); SWAP(4, 5);
			SWAP(0, 2); SWAP(3, 5);
			SWAP(0, 1); SWAP(3, 4); SWAP(2, 5);
			SWAP(0, 3); SWAP(1, 4);
			SWAP(2, 4); SWAP(1, 3);
			SWAP(2, 3);

		} else if (range.length() == 5) {
			SWAP(0, 1); SWAP(3, 4);
			SWAP(2, 4);
			SWAP(2, 3); SWAP(1, 4);
			SWAP(0, 3);
			SWAP(0, 2); SWAP(1, 3);
			SWAP(1, 2);

		} else if (range.length() == 4) {
			SWAP(0, 1); SWAP(2, 3);
			SWAP(0, 2); SWAP(1, 3);
			SWAP(1, 2);
		}

		#undef SWAP
	}
	if (size < 8) return;

	Cache<T> cache_obj (size);
	T * cache = cache_obj.cache;
	const size_t cache_size = cache_obj.cache_size;

	while (true) {
		if (iterator.length() < cache_size) {
			if ((iterator.length() + 1) * 4 <= cache_size && iterator.length() * 4 <= size) {
				iterator.begin();
				while (!iterator.finished()) {
					Range<I> A1 = iterator.nextRange(first);
					Range<I> B1 = iterator.nextRange(first);
					Range<I> A2 = iterator.nextRange(first);
					Range<I> B2 = iterator.nextRange(first);

					if (compare(*(B1.end - 1), *A1.start)) {
						// the two ranges are in reverse order, so copy them in reverse order into the cache
						std::copy(A1.start, A1.end, cache + B1.length());
						std::copy(B1.start, B1.end, cache);
					} else if (compare(*B1.start, *(A1.end - 1))) {
						// these two ranges weren't already in order, so merge them into the cache
						std::merge(A1.start, A1.end, B1.start, B1.end, cache, compare);
					} else {
						// if A1, B1, A2, and B2 are all in order, skip doing anything else
						if (!compare(*B2.start, *(A2.end - 1)) &&
							!compare(*A2.start, *(B1.end - 1))) continue;

						// copy A1 and B1 into the cache in the same order
						std::copy(A1.start, A1.end, cache);
						std::copy(B1.start, B1.end, cache + A1.length());
					}
					A1 = Range<I>(A1.start, B1.end);

					// merge A2 and B2 into the cache
					if (compare(*(B2.end - 1), *A2.start)) {
						// the two ranges are in reverse order, so copy them in reverse order into the cache
						std::copy(A2.start, A2.end, cache + A1.length() + B2.length());
						std::copy(B2.start, B2.end, cache + A1.length());
					} else if (compare(*B2.start, *(A2.end - 1))) {
						// these two ranges weren't already in order, so merge them into the cache
						std::merge(A2.start, A2.end, B2.start, B2.end, cache + A1.length(), compare);
					} else {
						// copy A2 and B2 into the cache in the same order
						std::copy(A2.start, A2.end, cache + A1.length());
						std::copy(B2.start, B2.end, cache + A1.length() + A2.length());
					}
					A2 = Range<I>(A2.start, B2.end);

					// merge A1 and A2 from the cache into the array
					Range<T*> A3(cache, cache + A1.length());
					Range<T*> B3(cache + A1.length(), cache + A1.length() + A2.length());

					if (compare(*(B3.end - 1), *A3.start)) {
						// the two ranges are in reverse order, so copy them in reverse order into the array
						std::copy(A3.start, A3.end, A1.start + A2.length());
						std::copy(B3.start, B3.end, A1.start);
					} else if (compare(*B3.start, *(A3.end - 1))) {
						// these two ranges weren't already in order, so merge them back into the array
						std::merge(A3.start, A3.end, B3.start, B3.end, A1.start, compare);
					} else {
						// copy A3 and B3 into the array in the same order
						std::copy(A3.start, A3.end, A1.start);
						std::copy(B3.start, B3.end, A1.start + A1.length());
					}
				}

				iterator.nextLevel();

			} else {
				iterator.begin();
				while (!iterator.finished()) {
					Range<I> A = iterator.nextRange(first);
					Range<I> B = iterator.nextRange(first);

					if (compare(*(B.end - 1), *A.start)) {
						// the two ranges are in reverse order, so a simple rotation should fix it
						std::rotate(A.start, A.end, B.end);
					} else if (compare(*B.start, *(A.end - 1))) {
						// these two ranges weren't already in order, so we'll need to merge them!
						std::copy(A.start, A.end, cache);
						MergeExternal(A.start, A.end, B.start, B.end, cache, compare);
					}
				}
			}
		} else {
			std::size_t block_size = std::sqrt(iterator.length());
			std::size_t buffer_size = iterator.length()/block_size + 1;

			// as an optimization, we really only need to pull out the internal buffers once for each level of merges
			// after that we can reuse the same buffers over and over, then redistribute it when we're finished with this level
			Range<I> buffer1(first, first);
			Range<I> buffer2(first, first);
			I index, last;
			std::size_t count, pull_index = 0;
			struct
			{
				I from, to;
				std::size_t count;
				Range<I> range;
			} pull[2];
			pull[0].count = 0; pull[0].range = Range<I>(first, first);
			pull[1].count = 0; pull[1].range = Range<I>(first, first);

			// find two internal buffers of size 'buffer_size' each
			// let's try finding both buffers at the same time from a single A or B subarray
			std::size_t find = buffer_size + buffer_size;
			bool find_separately = false;

			if (block_size <= cache_size) {
				find = buffer_size;
			} else if (find > iterator.length()) {
				find = buffer_size;
				find_separately = true;
			}

			iterator.begin();
			while (!iterator.finished()) {
				Range<I> A = iterator.nextRange(first);
				Range<I> B = iterator.nextRange(first);

				#define PULL(_to) \
					pull[pull_index].range = Range<I>(A.start, B.end); \
					pull[pull_index].count = count; \
					pull[pull_index].from = index; \
					pull[pull_index].to = _to


				for (last = A.start, count = 1; count < find; last = index, ++count) {
					index = FindLastForward(last + 1, A.end, *last, compare, find - count);
					if (index == A.end) break;
					assert(index < A.end);
				}
				index = last;

				if (count >= buffer_size) {
					PULL(A.start);
					pull_index = 1;

					if (count == buffer_size + buffer_size) {
						buffer1 = Range<I>(A.start, A.start + buffer_size);
						buffer2 = Range<I>(A.start + buffer_size, A.start + count);
						break;
					} else if (find == buffer_size + buffer_size) {
						buffer1 = Range<I>(A.start, A.start + count);
						find = buffer_size;
					} else if (block_size <= cache_size) {
						buffer1 = Range<I>(A.start, A.start + count);
						break;
					} else if (find_separately) {
						buffer1 = Range<I>(A.start, A.start + count);
						find_separately = false;
					} else {
						buffer2 = Range<I>(A.start, A.start + count);
						break;
					}
				} else if (pull_index == 0 && count > buffer1.length()) {
					buffer1 = Range<I>(A.start, A.start + count);
					PULL(A.start);
				}

					for (last = B.end - 1, count = 1; count < find; last = index - 1, ++count) {
						index = FindFirstBackward(B.start, last, *last, compare, find - count);
						if (index == B.start) break;
						assert(index > B.start);
					}
					index = last;

				if (count >= buffer_size) {
					PULL(B.end);
					pull_index = 1;

					if (count == buffer_size + buffer_size) {
						// we were able to find a single contiguous section containing 2√A unique values,
						// so this section can be used to contain both of the internal buffers we'll need
						buffer1 = Range<I>(B.end - count, B.end - buffer_size);
						buffer2 = Range<I>(B.end - buffer_size, B.end);
						break;
					} else if (find == buffer_size + buffer_size) {
						// we found a buffer that contains at least √A unique values, but did not contain the full 2√A unique values,
						// so we still need to find a second separate buffer of at least √A unique values
						buffer1 = Range<I>(B.end - count, B.end);
						find = buffer_size;
					} else if (block_size <= cache_size) {
						// we found the first and only internal buffer that we need, so we're done!
						buffer1 = Range<I>(B.end - count, B.end );
						break;
					} else if (find_separately) {
						// found one buffer, but now find the other one
						buffer1 = Range<I>(B.end - count, B.end);
						find_separately = false;
					} else {
						// buffer2 will be pulled out from a 'B' subarray, so if the first buffer was pulled out from the corresponding 'A' subarray,
						// we need to adjust the end point for that A subarray so it knows to stop redistributing its values before reaching buffer2
						if (pull[0].range.start == A.start) {
							pull[0].range.end -= pull[1].count;
						}

						// we found a second buffer in a 'B' subarray containing √A unique values, so we're done!
						buffer2 = Range<I>(B.end - count, B.end);
						break;
					}
				} else if (pull_index == 0 && count > buffer1.length()) {
					// keep track of the largest buffer we were able to find
					buffer1 = Range<I>(B.end - count, B.end);
					PULL(B.end);
				}

				#undef PULL
			}

			for (pull_index = 0; pull_index < 2; ++pull_index) {
				std::size_t length = pull[pull_index].count;

				if (pull[pull_index].to < pull[pull_index].from) {
					index = pull[pull_index].from;
					for (count = 1; count < length; ++count) {
						index = FindFirstBackward(pull[pull_index].to, pull[pull_index].from - (count - 1), *(index - 1), 
							compare, length - count);
						Range<I> range(index + 1, pull[pull_index].from + 1);
						std::rotate(range.start, range.end - count, range.end);
						pull[pull_index].from = index + count;
					}
				}

				if (pull[pull_index].to > pull[pull_index].from) {
					index = pull[pull_index].from + 1;
					for (count = 1; count < length; ++count) {
						index = FindLastForward(index, pull[pull_index].to, *index,
							compare, length - count);
						Range<I> range(pull[pull_index].from, index - 1);
						std::rotate(range.start, range.start + count, range.end);
						pull[pull_index].from = index - count - 1;
					}
				}
			}

			buffer_size = buffer1.length();
			block_size = iterator.length() / buffer_size + 1;
			iterator.begin();

			while (!iterator.finished()) {
				Range<I> A = iterator.nextRange(first);
				Range<I> B = iterator.nextRange(first);

				{ 
					auto start = A.start == pull[0].range.start;
					/* adjust A start and B end */
					A.start += (start && (pull[0].from > pull[0].to)) ? pull[0].count : 0;
					B.end   -= (start && (pull[0].from < pull[0].to)) ? pull[0].count : 0;
				
					if (A.length() == 0 || B.length() == 0) continue;
				}
				{
					auto start = A.start == pull[1].range.start;
					A.start += (start && (pull[1].from > pull[1].to)) ? pull[1].count : 0;
					B.end   -= (start && (pull[1].from < pull[1].to)) ? pull[1].count : 0;
					if (A.length() == 0 || B.length() == 0) continue;			
				}

				if (compare(*(B.end - 1), *A.start)) {
					std::rotate(A.start, A.end, B.end);
				} else if (compare(*A.end, *(A.end - 1))) {
					Range<I> blockA(A);
					Range<I> firstA(A.start, A.start + blockA.length() % block_size);

					for (I indexA = buffer1.start, index = firstA.end;
						 index < blockA.end;
						 ++indexA, index += block_size) {
						std::iter_swap(indexA, index);
					}

					Range<I> lastA (firstA);
					Range<I> lastB (first, first);
					Range<I> blockB (B.start, B.start + std::min(block_size, B.length()));
					blockA.start += firstA.length();
					I indexA = buffer1.start;

					if (lastA.length() <= cache_size) {
						std::copy(lastA.start, lastA.end, cache);
					} else if (buffer2.length() > 0) {
						std::swap_ranges(lastA.start, lastA.end, buffer2.start);
					}

					if (blockA.length() > 0) {
						while (true) {
							if ((lastB.length() > 0 && !compare(*(lastB.end - 1), *indexA)) ||
								blockB.length() == 0) {
								I B_split = std::lower_bound(lastB.start, lastB.end, *indexA, compare);
								std::size_t B_remaining = std::distance(B_split, lastB.end);

								I minA = blockA.start;
								for (I findA = minA + block_size ; findA < blockA.end ; findA += block_size) {
									if (compare(*findA, *minA)) {
										minA = findA;
									}
								}
								std::swap_ranges(blockA.start, blockA.start + block_size, minA);

								std::iter_swap(blockA.start, indexA);
								++indexA;

								if (lastA.length() <= cache_size) {
									MergeExternal(lastA.start, lastA.end, lastA.end, B_split, cache, compare);
								} else if (buffer2.length() > 0) {
									MergeInternal(lastA.start, lastA.end, lastA.end, B_split, buffer2.start, compare);
								} else {
									MergeInPlace(lastA.start, lastA.end, lastA.end, B_split, compare);
								}

								if (buffer2.length() > 0 || block_size <= cache_size) {
									if (block_size <= cache_size) {
										std::copy(blockA.start, blockA.start + block_size, cache);
									} else {
										std::swap_ranges(blockA.start, blockA.start + block_size, buffer2.start);
									}
									std::swap_ranges(B_split, B_split + B_remaining, blockA.start + block_size - B_remaining);
								} else {
									std::rotate(B_split, blockA.start, blockA.start + block_size);
								}

								lastA = Range<I>(blockA.start - B_remaining, blockA.start - B_remaining + block_size);
								lastB = Range<I>(lastA.end, lastA.end + B_remaining);


								blockA.start += block_size;
								if (blockA.length() == 0) break;

							} else if (blockB.length() < block_size) {
								std::rotate(blockA.start, blockB.start, blockB.end);

								lastB = Range<I>(blockA.start, blockA.start + blockB.length());
								blockA.start += blockB.length();
								blockA.end += blockB.length();
								blockB.end = blockB.start;
							} else {
								std::swap_ranges(blockA.start, blockA.start + block_size, blockB.start);
								lastB = Range<I>(blockA.start, blockA.start + block_size);

								blockA.start += block_size;
								blockA.end += block_size;
								blockB.start += block_size;

								if (blockB.end > B.end - block_size) {
									blockB.end = B.end;
								} else {
									blockB.end += block_size;
								}
							}
						}
					}

					if (lastA.length() <= cache_size) {
						MergeExternal(lastA.start, lastA.end, lastA.end, B.end, cache, compare);
					} else if (buffer2.length() > 0) {
						MergeInternal(lastA.start, lastA.end, lastA.end, B.end, buffer2.start, compare);
					} else {
						MergeInPlace(lastA.start, lastA.end, lastA.end, B.end, compare);
					}
				}
			}

			insertionsort(buffer2.start, buffer2.end, std::is_sorted_until(buffer2.start, buffer2.end));

			for (pull_index = 0 ; pull_index < 2 ; ++pull_index) {
				std::size_t unique = pull[pull_index].count * 2;
				if (pull[pull_index].from > pull[pull_index].to) {
					Range<I> buffer(
						pull[pull_index].range.start,
						pull[pull_index].range.start + pull[pull_index].count
					);
					while (buffer.length() > 0) {
						index = FindFirstForward(buffer.end, pull[pull_index].range.end,
												 *buffer.start, compare, unique);
						std::size_t amount = index - buffer.end;
						std::rotate(buffer.start, buffer.end, index);
						buffer.start += (amount + 1);
						buffer.end += amount;
						unique -= 2;
					}
				} else if (pull[pull_index].from < pull[pull_index].to) {
					Range<I> buffer(
						pull[pull_index].range.end - pull[pull_index].count,
						pull[pull_index].range.end
					);
					while (buffer.length() > 0) {
						index = FindLastBackward(pull[pull_index].range.start, buffer.start,
												 *(buffer.end - 1), compare, unique);
						std::size_t amount = buffer.start - index;
						std::rotate(index, index + amount, buffer.end);
						buffer.start -= amount;
						buffer.end -= (amount + 1);
						unique -= 2;
					}
				}
			}
		}

		if (!iterator.nextLevel()) break;
	}
}


