#ifndef UUID_1c4f302b_4a8c_45d4_96ff_c106ed5d884b
#define UUID_1c4f302b_4a8c_45d4_96ff_c106ed5d884b

#include <iostream>
#include <algorithm>
#include <memory>
#include <vector>
#include "search.h"
#include "insertionsort.h"

#define limit 32

template<class I>
void merge(I begin, I mid, I end) noexcept {
	typedef class std::iterator_traits<I>::value_type V;

	begin = lower_bound(begin, mid, *mid);

	std::vector<V> buffer(std::distance(begin, end));

	auto left = begin;
	auto right = mid;
	auto next = buffer.begin();

	while (left < mid && right < end) {
		while (*left <= *right && left < mid && right < end) {
			*next = *left;
			++left; ++next;
		} 

		if (left < mid && right < end) {
			*next = *right;
			++right; ++next;
		}
	}

	std::copy_backward(left, mid, end);
//	next = std::copy(left, mid, next);
	std::copy(buffer.begin(), next, begin);

}

#endif
