.PHONY: clean all
.INTERMEDIATE: data/%-reverse.data

CXXFLAGS=-std=c++11 -O3
CC=$(CXX)
VPATH=src tex
bins=bin/random bin/sorted bin/semi_sorted bin/interleaved bin/cut bin/sin
data=data/random.data data/sorted.data data/semi_sorted.data data/interleaved.data data/cut.data data/sin.data
grafs=graf/random.png graf/sorted.png graf/semi_sorted.png graf/interleaved.png graf/cut.png graf/sin.png

bins-reverse=bin/sorted-reverse bin/semi_sorted-reverse bin/interleaved-reverse bin/cut-reverse bin/sin-reverse
data-reverse=data/sorted-reverse.data data/semi_sorted-reverse.data data/interleaved-reverse.data data/cut-reverse.data data/sin-reverse.data
grafs-reverse=graf/sorted-reverse.png graf/semi_sorted-reverse.png graf/interleaved-reverse.png graf/cut-reverse.png graf/sin-reverse.png

all: graf bin data |  $(bins) $(data) $(grafs) $(bins-reverse) $(data-reverse) $(grafs-reverse)

graf/%.png: data/%.data plot.gp
	NAME=`basename $< .data` gnuplot plot.gp

bin/%-reverse: %.o main_reverse.o cutter.o
	$(CXX) -o $@ $^

bin/%: %.o main.o cutter.o
	$(CXX) -o $@ $^

data/%.data: bin/%
	$< > $@

graf:
	mkdir graf

data:
	mkdir data

bin:
	mkdir bin

main-reverse.o: main_reverse.cc sort.h insertionsort.h integer.h merge.h mergesort.h search.h blocksort.h
main.o: main.cc sort.h insertionsort.h integer.h merge.h mergesort.h search.h blocksort.h
%.o: %.cc integer.h

clean:
	$(RM) graf/* bin/* data/* *.o

